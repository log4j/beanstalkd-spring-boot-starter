# beanstalkd-spring-boot-starter

#### 介绍

Beanstalkd是一个简单、高效的工作队列系统，其最初设计目的是通过后台异步执行耗时任务方式降低高容量Web应用的页面延时。而其简单、轻量、易用等特点，和对任务优先级、延时 超时重发等控制，以及众多语言版本的客户端的良好支持，使其可以很好的在各种需要队列系统的场景中应用。

#### 引入使用

```
       <dependency>
            <groupId>com.pig4cloud.beanstalk</groupId>
            <artifactId>beanstalkd-client-spring-boot-starter</artifactId>
            <version>0.0.1</version>
        </dependency>
```


